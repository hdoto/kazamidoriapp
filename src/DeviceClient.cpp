#include "DeviceClient.h"

#include <limits.h>

#ifdef __APPLE__
  #include "wiringDummy.h"
#elif __linux__
  #include "wiringPi.h"
#endif

#define PIN_NSLEEP 4
#define PIN_DIR    27
#define PIN_STEP   17
#define PIN_PHOTO  22

#define MOT_STEPS 200
#define MOT_SPEED  60

#define DIR_RESET INT_MAX

DeviceClient::DeviceClient(long resetInterval) : ofThread() {
  this->resetInterval = resetInterval;
  this->prevResetTime = std::chrono::system_clock::now();

  wiringPiSetupGpio();

  // motor
  pinMode(PIN_NSLEEP, OUTPUT);
  pinMode(PIN_DIR,    OUTPUT);
  pinMode(PIN_STEP,   OUTPUT);

  // photo detector
  pinMode(PIN_PHOTO, INPUT);

  // reset & start thread
  reset();
  startThread();
}

DeviceClient::~DeviceClient() {
  queue.clear();
  stopThread();
}

void DeviceClient::rotate(int dir) { queue.push_back(dir); }

void DeviceClient::reset() { queue.push_back(DIR_RESET); }

void DeviceClient::savePower() {
  digitalWrite(PIN_NSLEEP, LOW);
}

void DeviceClient::_step(int steps) {
  int _steps = abs(steps);
  int _dir   = steps > 0 ? 1 : 0;
  int delay = 60 * 1000 / MOT_STEPS / MOT_SPEED; // = 3
  
  digitalWrite(PIN_NSLEEP, HIGH);
  digitalWrite(PIN_DIR, _dir);
  
  int i;
  for (i=0; i<_steps; i++) {
    digitalWrite(PIN_STEP, HIGH);
    usleep(2);
    digitalWrite(PIN_STEP, LOW);
    sleep(delay);
  }
  
  // digitalWrite(PIN_NSLEEP, LOW);
}

void DeviceClient::_rotate(int dir) {
  ofLogNotice("DeviceClient", "rotate: %d", dir);

  while (dir < 0) {
    dir += 360;
  }
  while (dir > 360) {
    dir -= 360;
  }

  // now, 0 <= dir < 360
  int steps = dir * MOT_STEPS / 360;
  int delta = steps - current;
  if (delta > MOT_STEPS / 2)
    delta -= MOT_STEPS;
  else if (delta < -MOT_STEPS / 2)
    delta += MOT_STEPS;

  sleep(250);
  if (delta == 0) {
    _step(5);
    sleep(250);
    _step(-10);
    sleep(250);
    _step(10);
    sleep(250);
    _step(-5);
  } else {
    _step(delta);
  }

  current = steps;
  sleep(250);
  savePower();
}

void DeviceClient::_reset() {
  ofLogNotice("DeviceClient", "reset");

  int i;
  for (i = 0; i <= MOT_STEPS; i++) {
    if (digitalRead(PIN_PHOTO) == LOW) {
      break;
    }
    _step(1);
  }

  if (i >= MOT_STEPS) {
    ofLogWarning("DeviceClient", "error: can not reset the device");
  }

  current = 0;
  sleep(250);
  savePower();
  
}

void DeviceClient::threadedFunction() {
  while (isThreadRunning()) {
    double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
                         std::chrono::system_clock::now() - prevResetTime)
                         .count();
    if (elapsed > resetInterval) {
      reset();
      prevResetTime = std::chrono::system_clock::now();
    }

    if (queue.size() > 0) {
      int dir = queue[0];
      // ofLogNotice("DeviceClient", "queue: %d", dir);
      queue.erase(queue.begin());
      if (dir == DIR_RESET) {
        this->_reset();
      } else {
        this->_rotate(dir);
      }
    } else {
      sleep(100);
    }
  }
}

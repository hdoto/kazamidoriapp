#pragma once

#include "ofMain.h"

struct DeviceResult {
  string source;
};

class DeviceClient : public ofThread {
 public:
  DeviceClient(long resetInterval);
  virtual ~DeviceClient();

  void rotate(int dir);
  void reset();
  void savePower();

  void threadedFunction();
  ofEvent<DeviceResult> resultEvent;

 private:
  void _rotate(int dir);
  void _reset();
  void _step(int steps);

  long resetInterval;
  std::chrono::system_clock::time_point prevResetTime;

  vector<int> queue;
  int current;
};

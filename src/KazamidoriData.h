#pragma once

#include "ofMain.h"

struct KazamidoriData {
  long date;
  string address;
  string latitude;
  string longitude;
  int angle;
  string location;
  string source;

  int compare(KazamidoriData data) { return source.compare(data.source); }
};

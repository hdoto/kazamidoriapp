#include "LocationDisplay.h"

const string PREFIX_TEXT = "from";

LocationDisplay::LocationDisplay(int fontSize) {
  prefixFont.loadFont("DINNextLTPro-UltraLight.otf", fontSize);
  locationFont.loadFont("DINNextLTPro-Bold.otf", fontSize);
  prefixWidth = prefixFont.stringWidth(PREFIX_TEXT) + prefixFont.getFontSize();
}

LocationDisplay::~LocationDisplay() {}

void LocationDisplay::draw() {
  if (locationText.length() <= 0) {
    return;
  }

  // draw location
  float locationWidth = locationFont.stringWidth(locationText);
  float triangleWidth = prefixFont.getFontSize();
  float x =
      (ofGetWindowWidth() - prefixWidth - locationWidth - triangleWidth) / 2;
  float y = ofGetWindowHeight() / 2;
  ofFill();
  // draw triangle
  ofSetColor(153);
  ofDrawTriangle(x, y - triangleWidth / 8, x + triangleWidth / 2,
                 y - triangleWidth / 8 - triangleWidth / 4, x,
                 y - triangleWidth / 8 - triangleWidth / 2);
  // draw prefix text
  prefixFont.drawString(PREFIX_TEXT, x + triangleWidth, y);
  // draw location text
  ofSetColor(0);
  locationFont.drawString(locationText, x + triangleWidth + prefixWidth, y);
}

void LocationDisplay::setLocation(string locationText) {
  this->locationText = locationText;
}
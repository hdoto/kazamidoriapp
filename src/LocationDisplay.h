#pragma once

#include "ofMain.h"
#include "ofxTrueTypeFontUC.h"

class LocationDisplay {
 public:
  LocationDisplay(int fontSize);
  virtual ~LocationDisplay();

  void draw();
  void setLocation(string locationText);

 private:
  ofxTrueTypeFontUC prefixFont;
  ofxTrueTypeFontUC locationFont;
  string locationText = "";
  float prefixWidth;
};

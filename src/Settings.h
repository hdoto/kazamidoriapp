#pragma once

#include "ofxXmlSettings.h"

class Settings {
 public:
  struct SERVER {
    string url;
    int requestInterval;
    string dummyFile;
    int dummyInterval;
  } server;

  struct DEVICE {
    int resetInterval;
  } device;

  struct DISPLAY {
    float fontSize;
  } display;

  void load(string file) {
    ofxXmlSettings settings;
    settings.loadFile(file);

    // server
    server.url = settings.getValue(
        "settings:server:url",
        "http://proj.howeb.org/kazamidori/howeb/data/data.csv");
    server.requestInterval =
        settings.getValue("settings:server:requestInterval", 30000);
    server.dummyFile =
        settings.getValue("settings:server:dummyFile", "dummies.csv");
    server.dummyInterval =
        settings.getValue("settings:server:dummyInterval", 30000);
    // device
    device.resetInterval =
        settings.getValue("settings:device:resetInterval", 3600000);
    // display
    display.fontSize = settings.getValue("settings:display:fontSize", 48.0);
  };

  void save(string file) {
    ofxXmlSettings settings;

    // server
    settings.setValue("settings:server:url", server.url);
    settings.setValue("settings:server:requestInterval",
                      server.requestInterval);
    settings.setValue("settings:server:dummyFile", server.dummyFile);
    settings.setValue("settings:server:dummyInterval", server.dummyInterval);
    // device
    settings.setValue("settings:device:resetInterval", device.resetInterval);
    // display
    settings.setValue("settings:display:fontSize", display.fontSize);

    settings.saveFile(file);
  };
};

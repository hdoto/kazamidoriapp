#include "WebClient.h"

#include <random>

WebClient::WebClient(string url, long interval) : ofThread() {
  this->url = url;
  this->requestInterval = interval;
  ofAddListener(httpUtils.newResponseEvent, this, &WebClient::newResponse);
  httpUtils.start();
  startThread();
}

WebClient::~WebClient() {
  httpUtils.stop();
  stopThread();
}

KazamidoriData parse(string line) {
  // example:
  // 1255196526,65.55.107.213,47.6742,-122.115,273,Tokyo Japan
  KazamidoriData result;
  result.source = ofTrim(line);
  vector<string> items = ofSplitString(result.source, ",");
  if (items.size() == 6) {
    result.date = stol(items[0]);
    result.address = ofTrim(items[1]);
    result.latitude = ofTrim(items[2]);
    result.longitude = ofTrim(items[3]);
    result.angle = stoi(items[4]);
    result.location = ofTrim(items[5]);
  }
  return result;
}

void WebClient::loadDummies(string file, long interval) {
  ofBuffer buffer = ofBufferFromFile(file);
  dummies.clear();
  for (auto line : buffer.getLines()) {
    if (line.size() <= 5) {
      continue;
    }
    KazamidoriData data = parse(line);
    dummies.push_back(data);
  }

  this->dummyInterval = interval;
  this->prevDisplayTime = std::chrono::system_clock::now();
}

int random(int max, int prev) {
  random_device rnd;
  mt19937 mt(rnd());
  uniform_int_distribution<> rand(0, max);
  int num = rand(mt);
  if (max > 0 && prev == num) {
    num = random(max, prev);
  }
  return num;
}

void WebClient::threadedFunction() {
  while (isThreadRunning()) {
    // retrieve from server
    httpUtils.getUrl(url);
    // wait for interval
    sleep(requestInterval / 2);
    // dummy check
    double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
                         std::chrono::system_clock::now() - prevDisplayTime)
                         .count();
    if (elapsed > dummyInterval) {
      prevDummyId = random(dummies.size() - 1, prevDummyId);
      ofLogNotice("WebClient", "dummy: %d", prevDummyId);
      ofNotifyEvent(resultEvent, dummies[prevDummyId]);
      this->prevDisplayTime = std::chrono::system_clock::now();
    }
    // wait for interval
    sleep(requestInterval / 2);
  }
}

void WebClient::newResponse(ofxHttpResponse& response) {
  // check error
  ofLogNotice("WebClient", "response: %d %s", response.status,
              response.reasonForStatus.c_str());
  if (response.status != 200) {
    ofLogWarning("WebClient", "error and exit");
    return;
  }

  // notify result
  KazamidoriData data = parse((string)response.responseBody);
  if (data.compare(prevData) != 0) {
    ofNotifyEvent(resultEvent, data);
    this->prevDisplayTime = std::chrono::system_clock::now();
    prevData = data;
  }
}

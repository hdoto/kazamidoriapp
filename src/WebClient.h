#pragma once

#include "KazamidoriData.h"
#include "ofMain.h"
#include "ofxHttpUtils.h"

class WebClient : public ofThread {
 public:
  WebClient(string url, long interval);
  virtual ~WebClient();
  void loadDummies(string file, long interval);

  void threadedFunction();
  void newResponse(ofxHttpResponse& response);
  ofEvent<KazamidoriData> resultEvent;

 private:
  string url;
  long requestInterval;
  ofxHttpUtils httpUtils;
  KazamidoriData prevData;

  int dummyInterval;
  int prevDummyId;
  std::chrono::system_clock::time_point prevDisplayTime;
  vector<KazamidoriData> dummies;
};

#include "ofApp.h"
#include "ofMain.h"

//========================================================================
int main() {
  ofSetupOpenGL(1024, 768, OF_FULLSCREEN);
  ofRunApp(new ofApp());
}

#include "ofApp.h"

const string SETTINGS_FILE = "kazamidori.xml";

//--------------------------------------------------------------
void ofApp::setup() {
  // settings
  settings.load(SETTINGS_FILE);

  // window settings
  ofSetVerticalSync(true);
  ofSetBackgroundColor(255);
  ofSetFrameRate(30);

  // location display
  display = new LocationDisplay(settings.display.fontSize);

  // web client
  web = new WebClient(settings.server.url, settings.server.requestInterval);
  web->loadDummies(settings.server.dummyFile, settings.server.dummyInterval);
  ofAddListener(web->resultEvent, this, &ofApp::onKazamidoriDataReceived);

  // device client
  device = new DeviceClient(settings.device.resetInterval);
}

//--------------------------------------------------------------
void ofApp::exit() {
  ofRemoveListener(web->resultEvent, this, &ofApp::onKazamidoriDataReceived);
}

//--------------------------------------------------------------
void ofApp::update() {}

//--------------------------------------------------------------
void ofApp::draw() {
  // draw texts
  display->draw();
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {
  // r -> reset the device
  if (key == 'r') {
    device->reset();
  }

  // number + return -> rotate the device with the angle
  if (key == '-') {
    sign = -1;
    angle = 0;
  }
  else if (key >= '0' && key <= '9') {
    int n = (int) (key - '0');
    angle = angle * 10 + n;
  }
  else {
    if (key == OF_KEY_RETURN || key == 10) {
      device->rotate(sign * angle);
    }
    sign = 1;
    angle = 0;
  }
}

//--------------------------------------------------------------
void ofApp::onKazamidoriDataReceived(KazamidoriData &result) {
  ofLogNotice("ofApp", "location: %s (%d)", result.location.c_str(),
              result.angle);
  // update text
  display->setLocation(result.location);
  // change angle
  device->rotate(result.angle);
}

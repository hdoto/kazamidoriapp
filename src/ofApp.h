#pragma once

#include "ofMain.h"

#include "DeviceClient.h"
#include "LocationDisplay.h"
#include "Settings.h"
#include "WebClient.h"

class ofApp : public ofBaseApp {
 public:
  void setup();
  void exit();
  void update();
  void draw();
  void keyReleased(int key);
  void onKazamidoriDataReceived(KazamidoriData &result);

 private:
  Settings settings;
  LocationDisplay *display;
  WebClient *web;
  DeviceClient *device;
  int angle = 0;
  int sign = 1;
};
